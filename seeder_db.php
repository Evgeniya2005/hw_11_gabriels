<?php
require_once 'config/db.php';
$members=[
  [
  'full_name' => 'Tom Khoy',
  'phone' => '0987654433',
  'email' => 'tom@gmail.com',
  'role' => 'student',
  'averange_mark' => '4',
  'working_day' => '',
  'subject' => '',
  ],
	[
  'full_name' => 'Dili Fyt',
  'phone' => '0960984433',
  'email' => 'dili@gmail.com',
  'role' => 'administrator',
  'working_day' => 'monday',
  'averange_mark' => '',
  'subject' => '',
  ],
	[
  'full_name' => 'David Malow',
  'phone' => '0960925633',
  'email' => 'david@gmail.com',
  'role' => 'teacher',
  'subject' => 'php',
  'averange_mark' => '',
  'working_day' => '',
  ],	
];
$sql ='INSERT INTO member SET
full_name=:full_name,
phone =:phone,
email =:email,
role =:role,
averange_mark=:averange_mark,
subject=:subject,
working_day=:working_day';

try{
  foreach ($members as $member) {
    $memberObj=$db->prepare($sql);
    $memberObj->bindValue(':full_name',$member['full_name']);
    $memberObj->bindValue(':phone',$member['phone']);
    $memberObj->bindValue(':email',$member['email']);
    $memberObj->bindValue(':role',$member['role']);
    $memberObj->bindValue(':averange_mark',$member['averange_mark']);
    $memberObj->bindValue(':subject',$member['subject']);
    $memberObj->bindValue(':working_day',$member['working_day']);
    $memberObj->execute();
      }
}catch(Exception $e){
  $message='Database create test members error'.$e;
  die($message);
}

?>