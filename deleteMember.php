<?php
require_once 'config/db.php';

try {
	$sql = "DELETE FROM memeber WHERE id=:id";	 
	$deleteObj = $db->prepare($sql);
	$deleteObj->bindValue(':id',$_POST['memberId']);
	$deleteObj->execute();
} catch (Exception $e) {
	$message = "Error deleting member" . $e->getMessage();
	die($message);
}

header('Location:index.php');